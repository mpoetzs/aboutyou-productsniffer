package de.webdata.solution.test.aboutyou_parser;

import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
/**
 * parses aboutyou.com and extract products
 * 
 * @author mpoetzs
 *
 */
public class Parser {

	private final List<String> keywords;

	public Parser(List<String> keywords) {
		this.keywords = keywords;
	}

	public Offers getOffers() {

		Offers offers = new Offers();

		try {
			Document mainPage = Jsoup.connect(getUrlMainPageWithKeywordsToSearch()).get();
			Statistics.countRequest();

			Elements articles = mainPage.select("article");

			articles.parallelStream().forEach((article) -> {

				Offer offer = new Offer();

				Elements itempropName = article.select("div[itemprop=name]");
				offer.setName(itempropName.first().text());

				Elements itempropBrand = article.select("div[itemprop=brand]");
				offer.setBrand(itempropBrand.first().text());

				Elements itempropInitialPrice = article.select("h5.price.isStriked");
				offer.setInitialPrice(itempropInitialPrice.select("span.price-data").text());

				Elements itempropPrice = article.select("h5.price.actual-price");
				offer.setPrice(itempropPrice.select("span.price-data").text());

				// getting Description and articleId from ProductDetailSite
				String productDetailSiteURL = article.select("div.product-image").select("a").first().attr("abs:href");
				Document productDetailSite = null;
				try {
					productDetailSite = Jsoup.connect(productDetailSiteURL).get();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Statistics.countRequest();

				Elements itempropDescription = productDetailSite.select("div[itemprop=description]");

				offer.setArticleId(itempropDescription.select("small.gray-light span").text());
				offer.setDescription(itempropDescription.select("p").text());

				offers.add(offer);

			});

		} catch (IOException e) {
			// TODO log me
			e.printStackTrace();
		}

		return offers;
	}

	private String getUrlMainPageWithKeywordsToSearch() {

		String url = "https://www.aboutyou.de/suche?term=";
		String lastParameter = "&category=20201";

		StringJoiner concatKeywords = new StringJoiner("+");
		for (String keyword : keywords) {
			concatKeywords.add(keyword);
		}

		return url + concatKeywords.toString() + lastParameter;
	}

}
