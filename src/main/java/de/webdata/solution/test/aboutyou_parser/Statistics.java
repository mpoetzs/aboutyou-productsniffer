package de.webdata.solution.test.aboutyou_parser;

import com.google.common.base.Stopwatch;

/**
 * handles the statistics to print them out on systemout
 * 
 * @author mpoetzs
 *
 */
public class Statistics {

	private static Stopwatch stopwatch = null;
	private static int requests = 0;
	private static int extractedOffers = 0;

	public static void startStopwatch() {
		stopwatch = Stopwatch.createStarted();
	}

	public static void stopStopwatch() {
		stopwatch.stop();
	}

	public static void countRequest() {
		requests++;
	}

	public static void setExtractedOffers(int size) {
		extractedOffers = size;
	}

	public static void printStatistics() {
		System.out.println("amount of triggered HTTP request: " + requests);
		System.out.println("run-time: " + stopwatch);
		System.out.println("Amount of extracted products:" + extractedOffers);
	}

}
