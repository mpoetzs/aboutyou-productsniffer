package de.webdata.solution.test.aboutyou_parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * application to extract productdetails from aboutyou.com, for more details look at "doc/Webdata Solutions_Test Assignment_2017.pdf"
 */
public class App {

	// later use CLI commons for nicer interface
	public static void main(String[] args) {

		Statistics.startStopwatch();

		final List<String> keywords = new ArrayList<String>(Arrays.asList(args));

		Parser parser = new Parser(keywords);
		Offers offers = parser.getOffers();

		printToXMLFile(offers);

		Statistics.setExtractedOffers(offers.getOffers().size());
		Statistics.stopStopwatch();
		Statistics.printStatistics();
		// TODO: memory Footprint
	}

	static void printToXMLFile(Offers offers) {

		try {
			JAXBContext context = JAXBContext.newInstance(Offers.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			File file = new File("offers.xml");
			marshaller.marshal(offers, file);

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
