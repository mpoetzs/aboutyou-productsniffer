package de.webdata.solution.test.aboutyou_parser;

import java.util.ArrayList;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * handles a list of Offer
 * 
 * @author mpoetzs
 *
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Offers {

	@XmlElement(name = "offer")
	private List<Offer> offers;

	public Offers() {
		offers = new ArrayList<Offer>();
	}

	public List<Offer> getOffers() {
		return offers;
	}

	public void add(Offer offer) {
		offers.add(offer);
	}
}
