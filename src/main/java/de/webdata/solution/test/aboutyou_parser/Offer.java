package de.webdata.solution.test.aboutyou_parser;

/**
 * dataobject for products of aboutyou.com
 * 
 * @author mpoetzs
 *
 */
public class Offer {

	private String name;
	private String brand;
	private String color;
	private String price;
	private String initialPrice;
	private String description;
	private String articleId;
	private String shippingCost;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getInitialPrice() {
		return initialPrice;
	}

	public void setInitialPrice(String initialPrice) {
		this.initialPrice = initialPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArticleId() {
		return articleId;
	}

	void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	@Override
	public String toString() {
		return "Offer [name=" + name + ", brand=" + brand + ", color=" + color + ", price=" + price + ", initialPrice="
				+ initialPrice + ", description=" + description + ", articleId=" + articleId + ", shippingCost="
				+ shippingCost + "]";
	}

}
